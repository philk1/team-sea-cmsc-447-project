<?php
//===============================================================================
/**
\mainpage Server-side Redistricting Algorithm
\section description_sec Description:

This program reorganizes the districts in a given state using an algorithm
that is based purely on population, and not political views.

After the algorithm is run, a JSON file containing new district data is output
and the supporting database is updated as required by the sponsor.

\section purpose_sec Purpose:
The purpose of this page is to document the server-side classes, functions, and variables of the redistricting algorithm.
*/
//===============================================================================

//////////
// MAIN //
//////////

$servername = "localhost";
$username = "root";
$password = "";

// Create connection
$conn = mysqli_connect($servername, $username, $password);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
//echo "Connected successfully\n";
mysqli_select_db($conn, "team sea 447 project");

//TEST BIT:
//Set to 0 if your NOT testing code. Enables straggler/reshape.
//Set to 1 if you ARE testing code. Disables straggler/reshape.
$testcode = 0;
$startingPoints = array();
/*Add the state your testing to the array.
	the program will freeze if the length of the array doesn't 
	match the number of districts that you're testing for.
*/
$startingPoints["AK"] = array(99951);
$startingPoints["AL"] = array(35203);
$startingPoints["AR"] = array(72736,72653);
$startingPoints["AZ"] = array(86514);
$startingPoints["CA"] = array(90650, 90011, 91331, 90201, 90250, 
92335, 90280, 92683, 92336, 94565, 
92704, 92503, 91911, 95076, 92804, 
92345, 90604, 91744, 93307, 92376, 
93033, 94112, 93257);//in progress
$startingPoints["CO"] = array(80427, 80234, 80204, 80019, 80903, 81522, 80737);
$startingPoints["CT"] = array('06902', '06379');
$startingPoints["DE"] = array(19975);
$startingPoints["FL"] = array(33012, 33186,33411);//in progress
$startingPoints["GA"] = array(31562, 31405, 30474,31643, 31050,
						30253, 30241, 30296, 30317, 30342,
						30071, 30816, 30176, 30752);
$startingPoints["HI"] = array(96797);
$startingPoints["IA"] = array(51241);
$startingPoints["ID"] = array(83254, 83805);
$startingPoints["IL"] = array(60653, 60611, 60640);
$startingPoints["IN"] = array(47906);
$startingPoints["KS"] = array(66002);
$startingPoints["KY"] = array(40508);
$startingPoints["LA"] = array(70001);//Corrupt kml file
$startingPoints["MA"] = array('01267','01119','01474','01952', '02657','01532','02790');
$startingPoints["MD"] = array(21531, 20886, 21864, 21087, 20687, 20706, 20759, 21202);
$startingPoints["ME"] = array('03905');
$startingPoints["MI"] = array(49938, 49635,49117,49424, 48197);
$startingPoints["MN"] = array(55725,56568,56144,55921);
$startingPoints["MO"] = array(63005); //any zip throws redistrict error.
$startingPoints["MS"] = array(39204);
$startingPoints["MT"] = array(59404);
$startingPoints["NC"] = array(28546, 28461, 28343);//in progress
$startingPoints["ND"] = array(58005);
$startingPoints["NE"] = array(68002);
$startingPoints["NH"] = array('00177');
$startingPoints["NJ"] = array('08204','08070','08102','08758');
$startingPoints["NM"] = array(87014);
$startingPoints["NV"] = array(89109);
$startingPoints["NY"] = array(11231);//in progress
$startingPoints["OH"] = array(45011,45233,43085,44108, 44030, 
						44107,44131,44052, 43432, 43533,
						45332,45504,45054);
$startingPoints["OK"] = array('00067',73660, 73142);
$startingPoints["OR"] = array(97121, 97211,97341,97415);
$startingPoints["PA"] = array(19030,19135, 19106,19013);
$startingPoints["RI"] = array('02802');
$startingPoints["SC"] = array(29658,29840,29356,29732);
$startingPoints["TN"] = array(37240,37069,38109,38135,38261,37031);//missing 2 zips(conflict with AR/KY)
$startingPoints["TX"] = array(79907,79772,79087,79225, 75503,75974,77640,77590, 78521,78557);//75247,75202,75251, 77004,78205,79821);//in progress
$startingPoints["UT"] = array(84111);
$startingPoints["VA"] = array(22202,22309,22181,22611,22554,23662);//not contiguous
$startingPoints["VT"] = array('05001');
$startingPoints["WA"] = array(98381,98516,98422,98168);
$startingPoints["WI"] = array(53158,53172,53097,53081,54173);
$startingPoints["WV"] = array(25425);


$stateFromFrontEnd = $_POST['state'];//replace with state abbrev for manual testing.
$num_distFromBackEnd = $_POST['num_dist'];//replace with number of districts for manual testing.
if(array_key_exists($stateFromFrontEnd, $startingPoints)){
	$state = new State($stateFromFrontEnd,$num_distFromBackEnd, $startingPoints[$stateFromFrontEnd]);
}else{
	$state = new State('MD',8, $startingPoints["MD"]);
}

$state->addZipsFromDataBase();
$state->formAllDistricts();
//echo "<br>";
//echo $state->toString(); //Uncomment for manual testing.

$outputArray = array();
foreach($state->zips as $zipNum => $zipObject){
	array_push($outputArray,array('Zip_Code'=>$zipNum, 'District' => $zipObject->districtNumber));
}

$output = json_encode($outputArray);
echo $output;
//file_put_contents('../kml/results.json', $output);

mysqli_close($conn);


//END MAIN


////////////////////////
// SUPPORTING CLASSES //
////////////////////////

//===============================================================================
/**
ZipCode class.
ZipCode class: Represents a zip code, not much to it. Just attributes and a to-string method for testing and such.
*/
//===============================================================================
class ZipCode {

	public $zip; /**< (int/string) for zipcode. */
	public $state; /**< (string) state name. */
	public $population; /**< (int) population value. */
	public $lat; /**< (double): latitude of zip code centroid. */
	public $lng; /**< (double): longitude of zip code centroid. */
	public $districtNumber; /**< (int): District number, starts with no value. */
	public $neighborList = array(); /**< (list): list of neighboring zip codes, read in from database. */

	/**
	Zip code constructor.
	Constructor method, initializes the all the attributes of the zip code except for its districtNumber.
	@param &zip
	@param &state
	@param &population
	@param &lat
	@param &lng
	*/
	public function __construct($zip, $state, $population, $lat, $lng){
		$this->zip = $zip; 
		$this->state = $state;
		$this->population = $population;
		$this->lat = $lat;
		$this->lng = $lng;
		$this->readNeighborList();
	}
	
	/**
	Read SQL database for neighboring zips.
	readNeighborList reads the neighbor table in the database, filling out the neighborList with the appropriate values.
	*/
	public function readNeighborList(){
		global $conn;
		global $stateFromFrontEnd;
		//query database for all zips
		$sqlSelect1 = "SELECT neighbor FROM `".$stateFromFrontEnd."_azip` WHERE `zip` = " . $this->zip;
		if ($result1 = mysqli_query($conn, $sqlSelect1)) {
			for($i=0; $i<mysqli_num_rows($result1); $i++){
				$row1 = mysqli_fetch_array($result1, MYSQLI_NUM);
				array_push($this->neighborList, $row1[0]);
			}
		}
		mysqli_free_result($result1);
		/*
		$sqlSelect1 = "SELECT zip,neighbor FROM `".$stateFromFrontEnd."_azip`";
		// Select queries return a resultset
		if ($result1 = mysqli_query($conn, $sqlSelect1)) {
			//printf("Select returned %d rows.\n", mysqli_num_rows($result));
			//numeric array
			for($i=0; $i<mysqli_num_rows($result1); $i++){
				$row1 = mysqli_fetch_array($result1, MYSQLI_NUM);
				if($this->zip == $row1[0]){
				
					$sqlSelect2 = "SELECT zip FROM `zip code list` WHERE `state` LIKE '".$stateFromFrontEnd."'";
					
					if($result2 = mysqli_query($conn, $sqlSelect2)){
					//echo "SECOND IF STATEMETN";
						for($j=0; $j<mysqli_num_rows($result2); $j++){
							//echo "SECOND FOR LOOP";
							$row2 = mysqli_fetch_array($result2, MYSQLI_NUM);
							if($row1[1]==$row2[0]){
								array_push($this->neighborList, $row1[1]);
							}
						}
					}
					mysqli_free_result($result2);
				}
				//printf ("%s %s\n", $row[0], $row[1]);
			}
			// free result set
			mysqli_free_result($result1);
		}
		*/
		//returns array of neighboring zipcodes
	}
	
	/**
	Zip code toString.
	toString returns an organized string that describes the zip code.
	Used in testing.
	@return $string
	*/
	public function toString(){
		$string = "Zip: ";
		$string .= $this->zip;
		$string .= "\tPopulation: ";
		$string .= $this->population;
		$string .+ "<br>";
		return $string;
		
	}

}

//===============================================================================
/**
District class.
District class: Represents a District, not much to it. All you can do to it is add ZipCodes.
*/			 
//===============================================================================
class District {
	public $state; /**< State name. */
	public $districtNumber; /**< District number. */
	public $zips = array(); /**< Array of zip codes. */
	public $currentPopulation = 0; /**< Current population. */
	
	/**
	District Constructor.
	Constructor method, initializes the name of its state and its district number.
	@param $state
	@param $districtNumber
	*/
	public function __construct($state, $districtNumber){
		$this->state = $state;
		$this->districtNumber = $districtNumber;
	}
	
	/**
	Add zip to District.
	addZip adds a zip code to the district, updating the currentPopulation value of this district and the districtNumber of the zip code.
	@param $z Zipcode object.
	*/
	public function addZip(ZipCode &$z){
		global $conn;
		$z->districtNumber = $this->districtNumber;
		$this->currentPopulation += $z->population;
		array_push($this->zips, $z->zip);
		$updateDistrict = "UPDATE `zip code list` SET district = '$this->districtNumber' WHERE zip = '$z->zip'";
		mysqli_query($conn, $updateDistrict);
	}
	
	/**
	Removes zip from district.
	removeZip removes a zip code from the district, updating the currentPopulation value of this district and setting the districtNumber of the zip code to null.
	@param $z Zipcode object to remove.
	*/
	public function removeZip(ZipCode &$z){
		$z->districtNumber = null;
		$this->currentPopulation -= $z->population;
		foreach($this->zips as $index => $zipNum){
			if($zipNum == $z->zip){
				$removeIndex = $index;
			}
		}
		unset($this->zips[$removeIndex]);
	}
	
	/**
	toString for a District.
	toString returns an organized string that describes the district and its zip codes.
	Used in testing.
	@return $string Results string.
	*/
	public function toString(){
		$string = "<br>District ";
		$string .= $this->districtNumber;
		$string .= "<br>Num of Zips in District object: ";
		$string .= count($this->zips);
		$string .= "<br>Total Population: ";
		$string .= $this->currentPopulation;
		$string .= "<br>";
		$counter = 0;
		foreach($this->zips as $value){
			if($counter%25 == 0){
				$string .= "<br>";
			}
			else{
				$string .= ", ";
			}
			$string .= $value;
			$counter++;
		}
		$string .= "<br>";
		
		return $string;
	}
}

//===============================================================================
/**
State class.
State class: Represents a state in the form of a graph.
Main state object containing all the required info and results for redistricting.
*/
//===============================================================================
class State {
	
	public $state; /**< (string): abbreviated name of the state ("MD", "VT", etc.) */
	public $numberOfDistricts; /**< (int): number of required Districts for the state. */
	public $zips = array(); /**< (associative array): dictionary of all ZipCodes in the state. */
	public $districts = array(); /**< (array): array of all District objecsts in the state. */
	public $population = 0; /**< (int): total population of the state. */
	public $desiredPopulationPerDistrict = 0; /**< (int): will be population/numberOfDistricts, once ZipCodes are added. See addZip method. */
	public $startingZipCodes = array(); /**< Initial zipcodes to check. */

	
	/**
	State Constructor.
	Constructor method, initializes the name of the state and the number of districts it is required to contain.
	@param $state
	@param $numberOfDistricts
	@param $startingZipCodes
	*/

	public function __construct($state, $numberOfDistricts, $startingZipCodes){
		$this->state = $state;
		$this->numberOfDistricts = $numberOfDistricts;
		$this->startingZipCodes = $startingZipCodes;
	}
	
	/**
	Gets data from SQL database.
	addZipsFromDataBase reads the database, adding each zip code to the $zips array using the addZip() method.
	@see addZip()
	*/
	public function addZipsFromDatabase(){
		global $conn; 
		global $stateFromFrontEnd;
		$sqlSelect = "SELECT zip,population,state,lat,lng FROM `zip code list` WHERE `state` LIKE '".$stateFromFrontEnd."'";
		/* Select queries return a resultset */
		if ($result = mysqli_query($conn, $sqlSelect)) {
			//printf("Select returned %d rows.\n", mysqli_num_rows($result));
			/* numeric array */
			for($i=0; $i<mysqli_num_rows($result); $i++){
				$row = mysqli_fetch_array($result, MYSQLI_NUM);
				if($row[2] == $this->state){
					$this->addZip($row[0], $row[2], $row[1], $row[3], $row[4]);
					//printf ("%s %s %s %s %s\n", $row[0], $row[1], $row[2], $row[3], $row[4]);
				}
			}

			/* free result set */
			mysqli_free_result($result);
		} else {
		printf("Error: %s\n", mysqli_sqlstate($conn));
		}
	}
	
	/**
	Adds new zip code to array.
	addZip adds a single zip code to the $zips array based on the given parameters
	@param $zipCode
	@param $state
	@param $population
	@param $lat
	@param $lng
	*/
	public function addZip($zipCode, $state, $population, $lat, $lng){
		$this->population += $population;
		$this->desiredPopulationPerDistrict = ($this->population)/($this->numberOfDistricts);
		$this->zips[$zipCode] = new ZipCode($zipCode, $state, $population, $lat, $lng);
	}
	
	/**
	Create all districts.
	formAllDistricts assigns each zip code to a district.
	After this method is run, the districts are ready for output.
	*/
	public function formAllDistricts(){
		$rejectQueues = array();
		global $testcode;
		// Initialize each district using the formDistrict() method and specified starting points (first pass).
		for($i=0; $i<$this->numberOfDistricts; $i++){
			//echo "Starting District " . $i . "<br>";
			if($i >= count($this->startingZipCodes)){
				//printf("In random loop on district %s\n", $i+1);
				$rejectQueues[$i+1] = $this->formDistrict($rejectQueues[$i][count($rejectQueues[$i])-1], $i+1);
			}else{
			$rejectQueues[$i+1] = $this->formDistrict($this->startingZipCodes[$i], $i+1);
			}
		}
		if(!$testcode){
			// After first pass is run, find all unassigned zip codes and assign them to an appropriate district.
			$this->assignStragglers();//UNCOMMENT
			
			//if($_POST['state'] != "MI"){
				// Now swap zips between districts using remaining queues. This evens out the populations.
				$this->reconsolidateDistricts($rejectQueues);//UNCOMMENT
			//}
		}
	}
	
	/**
	Creates a district.
	formDistrict creates a single district, using a breadth-first search algorithm and a specified starting points.
	An array containing each zip code that neighbors the district is returned.
	@param $startingZipCode
	@param $districtNumber
	@return $rejectQueue
	*/
	public function formDistrict($startingZipCode, $districtNumber){
		//echo $startingZipCode . ", ";
		$queue = array($startingZipCode);
		$this->districts[$districtNumber] = new District($this->state, $districtNumber);
		$rejectQueue = array();
		
		while (count($queue) != 0){
			/*///////
			if(count($this->districts[$districtNumber]->zips) > 3){
				$queueByDistance = array();
				$centroid = $this->calculateCentroid($districtNumber);
				foreach($queue as $zip){
					$distance = $this->calculateDistance($this->zips[$zip]->lat,$this->zips[$zip]->lng,$centroid[0],$centroid[1]);
					$queueByDistance[$zip] = $distance;
				}
				asort($queueByDistance);
				reset($queueByDistance);
				$currentZip = key($queueByDistance);
				//print_r($queueByDistance);
				//echo "<br> Added Zip: " . $currentZip;
				foreach($queue as $index => $zip){
					if($zip == $currentZip){
						$removeIndex = $index;
					}
				}
				unset($queue[$removeIndex]);
				/////////
				echo "Current Zip: " . $currentZip;
			}
			else{*/
				$currentZip = array_shift($queue);
				//echo "Current Zip: " . $currentZip;
			//}
			$this->districts[$districtNumber]->addZip($this->zips[$currentZip]);
			reset($queue);
			if($this->districts[$districtNumber]->currentPopulation >= $this->desiredPopulationPerDistrict){
				//echo "District " . $districtNumber . " finished.<br>";
				//echo "Queue size: " . count($queue) . "<br>";
				
				foreach($queue as $element){
					array_push($rejectQueue, $element);
				}
				return $rejectQueue;
			}
			
			foreach($this->zips[$currentZip]->neighborList as $neighbor){
				$neighborFoundInQueue = false;
				foreach($queue as $q){
					if($neighbor == $q){
						$neighborFoundInQueue = true;
					}
				}
				
				if($neighborFoundInQueue == false){
				
					$neighborFoundInRejectQueue = false;
					if(count($rejectQueue) > 0){
						foreach($rejectQueue as $rq){
							if($neighbor == $rq){
								$neighborFoundInRejectQueue = true;
							}
						}
					}
					
					if($neighborFoundInRejectQueue == false){
						if($this->zips[$neighbor]->districtNumber == null){
							array_push($queue, $neighbor);
						}
						else{
							array_push($rejectQueue, $neighbor);
						}
					}
				}
			}
		}
		return $rejectQueue;
	}
	
	/**
	Assigns remaining zips to districts.
	assignStragglers finds all unassigned zip codes and assigns each of them to a district.
	The district of a straggling zip code decided in one of two ways:
		1) Based on the districts of an unassigned zip code's neighbors, giving priority to districts with smaller populations.
		2) If this doesn't work (e.g. a zip codes is an island with no neighbors),
		   assign the zip code to the district of the assigned zip code closest to it, determined by a distance function.
	*/
	public function assignStragglers(){
		//create array of straggler zips
		$unassignedZips = array();
		foreach($this->zips as $key=>$value){
			if($value->districtNumber == null){
				array_push($unassignedZips, $key);
			}
		}
		
		$noMoreNeighborZips = false;
		while(count($unassignedZips) > 0){
			if($noMoreNeighborZips == false){
				$noMoreNeighborZips = true;
				foreach($unassignedZips as $index => $zipNumber){
					if(count($this->zips[$zipNumber]->neighborList) != 0){
						$assignedNeighbors = array();
						foreach($this->zips[$zipNumber]->neighborList as $neighbor){
							if($this->zips[$neighbor]->districtNumber != null){
								array_push($assignedNeighbors, $neighbor);
							}
						}
						if(count($assignedNeighbors) > 0){
							$neighborWithSmallestDistrict = $assignedNeighbors[0];
							$lowestNeighboringPopulation = $this->districts[$this->zips[$assignedNeighbors[0]]->districtNumber]->currentPopulation;
							foreach($assignedNeighbors as $neighbor){
								if($this->districts[$this->zips[$neighbor]->districtNumber]->currentPopulation <= $lowestNeighboringPopulation){
									$neighborWithSmallestDistrict = $neighbor;
									$lowestNeighboringPopulation = $this->districts[$this->zips[$neighbor]->districtNumber]->currentPopulation;
								}
							}
							$this->districts[$this->zips[$neighborWithSmallestDistrict]->districtNumber]->addZip($this->zips[$zipNumber]);
							unset($unassignedZips[$index]);
							$noMoreNeighborZips = false;
						}
					}
				}
			}
			else{
				foreach($unassignedZips as $index => $zipNumber){
					$distanceList = array();
					foreach($this->zips as $zipNum=>$zipObject){
						if($zipObject->districtNumber != null){
							$distanceList[$zipNum] = $this->calculateDistance($this->zips[$zipNumber]->lat, $this->zips[$zipNumber]->lng, $zipObject->lat, $zipObject->lng);
						}
					}
					asort($distanceList);
					reset($distanceList);
					$closestZip = key($distanceList);
					//echo $zipNumber . " is closest to " . $closestZip . "<br>";
					$this->districts[$this->zips[$closestZip]->districtNumber]->addZip($this->zips[$zipNumber]);
					unset($unassignedZips[$index]);
				}
			}
		}
	}
	
	/**
	Calculates distance between zips.
	calculateDistance simply calculates the geographic distance from one zip code to another, and returns the value.
	@param $latitude1
	@param $latitude2
	@param $longitude1
	@param $longitude2
	@return $miles
	*/
	public function calculateDistance($latitude1, $longitude1, $latitude2, $longitude2) {
		$theta = $longitude1 - $longitude2;
		$miles = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));
		$miles = acos($miles);
		$miles = rad2deg($miles);
		$miles = $miles * 60 * 1.1515;
		return $miles;
	}
	
	/**
	Evens out population of districts.
	reconsolidateDistricts evens out the populations of the districts, swapping zip codes between districts of varying size.
	The process is complete once the population of each district is within 10 percent of the state's desiredPopulationPerDistrict value.
	@param $rejectQueues
	*/
	public function reconsolidateDistricts($rejectQueues){
		$districtsAreUneven = true;
		
		$smallestDistrict = 0;
		$biggestDistrict = 0;
		while($districtsAreUneven == true){
			$districtPopulations = array();
			
			foreach($this->districts as $districtNum => $districtObj){
				$districtPopulations[$districtNum] = $districtObj->currentPopulation;
			}
			asort($districtPopulations);
			reset($districtPopulations);
			$smallestDistrict = key($districtPopulations);
			end($districtPopulations);
			$biggestDistrict = key($districtPopulations);
			reset($districtPopulations);
			
			if($this->districts[$biggestDistrict]->currentPopulation > $this->desiredPopulationPerDistrict*1.2 ||
			   $this->districts[$smallestDistrict]->currentPopulation < $this->desiredPopulationPerDistrict*.8){
			   foreach($districtPopulations as $district => $population){
					$newDistrictPopulations = array();
					foreach($this->districts as $districtNum => $districtObj){
						$newDistrictPopulations[$districtNum] = $districtObj->currentPopulation;
					}
					asort($newDistrictPopulations);
					//echo "Districts by population: ";
					//print_r($newDistrictPopulations);
					//echo "<br>";
					$rejectQueues[$district] = $this->resizeDistrict($district,$rejectQueues[$district]);
				}
			}
			else{
				$districtsAreUneven = false;
			}
		}
	}
	
	/**
	Swaps border zips to even out districts.
	resizeDistrict swaps zip codes from the given district with zip codes from larger neighboring districts,
	giving priority to the largest of its neighbors.
	It stops when the population of the district reaches the state's desiredPopulationPerDistrict value,
	or the district becomes larger than all of its neighboring districts.
	@param $districtNumber
	@param $rejectQueue
	@return $newRejectQueue
	*/
	public function resizeDistrict($districtNumber,$rejectQueue){
		$queue = array();
		for($i=1; $i<=$this->numberOfDistricts; $i++){
			$queue[$i] = array();
		}
		//echo "resizeDistrict() " . $districtNumber . "<br>";
		
		if(count($rejectQueue[0]) > 0){
			foreach($rejectQueue as $element){
				if($this->zips[$element]->districtNumber != $districtNumber){
					array_push($queue[$this->zips[$element]->districtNumber], $element);
				}
			}
		}
		
		$newRejectQueue = array();
		while (count($queue) > 0){
			$neighboringDistrictPops = array();
			foreach($queue as $districtNum => $subqueue){
				if (count($subqueue) > 0 && $this->districts[$districtNum]->currentPopulation > $this->desiredPopulationPerDistrict*.9){
					$neighboringDistrictPops[$districtNum] = $this->districts[$districtNum]->currentPopulation;
				}
			}
			
			arsort($neighboringDistrictPops);
			if(count($neighboringDistrictPops)==0){
				foreach($queue as $subqueue){
					foreach($subqueue as $zip){
						array_push($newRejectQueue,$zip);
					}
				}
				return $newRejectQueue;
			}
			
			reset($neighboringDistrictPops);
			$biggestNeighboringDistrict = key($neighboringDistrictPops);
			//echo "<br>District to eat into: " . $biggestNeighboringDistrict;
			//echo "<br><br>";
			if($this->districts[$districtNumber]->currentPopulation >= $this->districts[$biggestNeighboringDistrict]->currentPopulation
			|| $this->districts[$districtNumber]->currentPopulation >= $this->desiredPopulationPerDistrict){
				foreach($queue as $subqueue){
					foreach($subqueue as $zip){
						array_push($newRejectQueue,$zip);
					}
				}
				return $newRejectQueue;
			}
			
			$queueByDistance = array();
			$centroid = $this->calculateCentroid($districtNumber);
			foreach($queue[$biggestNeighboringDistrict] as $zip){
				$distance = $this->calculateDistance($this->zips[$zip]->lat,$this->zips[$zip]->lng,$centroid[0],$centroid[1]);
				$queueByDistance[$zip] = $distance;
			}
			asort($queueByDistance);
			reset($queueByDistance);
			$currentZip = key($queueByDistance);
			//print_r($queueByDistance);
			//echo "<br> Added Zip: " . $currentZip;
			foreach($queue[$biggestNeighboringDistrict] as $index => $zip){
				if($zip == $currentZip){
					$removeIndex = $index;
				}
			}
			unset($queue[$biggestNeighboringDistrict][$removeIndex]);
			
			//$currentZip = array_shift($queue[$biggestNeighboringDistrict]);
			//echo "checking neighbor contiguity...";
			if($this->checkNeighborContiguity($currentZip)){
				$this->districts[$this->zips[$currentZip]->districtNumber]->removeZip($this->zips[$currentZip]);
				$this->districts[$districtNumber]->addZip($this->zips[$currentZip]);
				
				foreach($this->zips[$currentZip]->neighborList as $neighbor){
					if($this->zips[$neighbor]->districtNumber != $districtNumber){
						$neighborFoundInQueue = false;
						foreach($queue[$this->zips[$neighbor]->districtNumber] as $zipNum){
							if ($neighbor == $zipNum){
								$neighborFoundInQueue = true;
							}
						}
						if($neighborFoundInQueue == false){
							array_push($queue[$this->zips[$neighbor]->districtNumber],$neighbor);
						}
					}
				}
			}
			else{
				array_push($newRejectQueue,$currentZip);
			}
		}
		
		foreach($queue as $subqueue){
			foreach($subqueue as $zip){
				array_push($newRejectQueue,$zip);
			}
		}
		
		return $newRejectQueue;
		
	}
	/**
	Resizes a district.
	@param $districtNumber
	@param $rejectQueue
	@return $newRejectQueue
	*/
	public function resizeDistrict2($districtNumber,$rejectQueue){
		$queue = array();
		//echo "resizeDistrict() " . $districtNumber . "<br>";
		
		if(count($rejectQueue[0]) > 0){
			foreach($rejectQueue as $zip){
				if($this->zips[$zip]->districtNumber != $districtNumber){
					array_push($queue, $zip);
				}
			}
		}
		
		$newRejectQueue = array();
		while (count($queue) > 0){
			$queueByDistance = array();
			$centroid = $this->calculateCentroid($districtNumber);
			foreach($queue as $zip){
				$distance = $this->calculateDistance($this->zips[$zip]->lat,$this->zips[$zip]->lng,$centroid[0],$centroid[1]);
				$queueByDistance[$zip] = $distance;
			}
			asort($queueByDistance);
			reset($queueByDistance);
			$currentZip = key($queueByDistance);
			//print_r($queueByDistance);
			//echo "<br> Added Zip: " . $currentZip;
			foreach($queue as $index => $zip){
				if($zip == $currentZip){
					$removeIndex = $index;
				}
			}
			unset($queue[$removeIndex]);
			//$currentZip = array_shift($queue);
			//echo "checking neighbor contiguity...";
			if($this->checkNeighborContiguity($currentZip) && $this->districts[$this->zips[$currentZip]->districtNumber]->currentPopulation > $this->desiredPopulationPerDistrict){
				$this->districts[$this->zips[$currentZip]->districtNumber]->removeZip($this->zips[$currentZip]);
				$this->districts[$districtNumber]->addZip($this->zips[$currentZip]);
				
				foreach($this->zips[$currentZip]->neighborList as $neighbor){
					if($this->zips[$neighbor]->districtNumber != $districtNumber){
						$neighborFoundInQueue = false;
						foreach($queue as $zipNum){
							if ($neighbor == $zipNum){
								$neighborFoundInQueue = true;
							}
						}
						if($neighborFoundInQueue == false){
							array_push($queue,$neighbor);
						}
					}
				}
			}
			else{
				array_push($newRejectQueue,$currentZip);
			}
		}
		
		foreach($queue as $zip){
			array_push($newRejectQueue,$zip);
		}
		
		return $newRejectQueue;
		
	}
	
	/**
	Checks contiguity of a district.
	checkNeighborContiguity performs a check to verify that a district has maintained contiguity after a zip code has been removed.
	This method is called by the resizeDistrict method in the process of swapping zip codes between districts.
	A boolean is returned - true if contiguity is maintained, false if not.
	@param $zipNum
	@return boolean True if contiguous, else false.
	*/
	public function checkNeighborContiguity($zipNum){
		$contiguous = true;
		$searchableZips = array();
		
		foreach($this->zips[$zipNum]->neighborList as $neighbor){
			if($this->zips[$neighbor]->districtNumber == $this->zips[$zipNum]->districtNumber){
				array_push($searchableZips,$neighbor);
			}
		}
		
		for($i=0;$i<count($searchableZips)-1;$i++){
			//echo "Finding neighbor path...";
			$contiguous = $this->findNeighborPath($searchableZips[$i],$searchableZips[$i+1],$zipNum,$searchableZips);
			if ($contiguous == false){
				return false;
			}
		}
		
		return $contiguous;
	}
	
	/**
	Finds neighbor path between zips.
	findNeighborPath searches a given array of zip codes using depth-first search in attempt to find a path between the two given zip codes.
	A boolean is returned - true if a path is found, false if not.
	@param $startZip
	@param $endZip
	@param $zipToBeRemoved
	@param $searchableZips
	@return boolean True if path is found, false otherwise.
	*/
	public function findNeighborPath($startZip,$endZip,$zipToBeRemoved,$searchableZips){
		$stack = array();
		array_push($stack,$startZip);
		$visitedZips = array();
		
		while(count($stack) > 0){
			$currentZip = array_pop($stack);
			array_push($visitedZips,$currentZip);
			
			if($currentZip == $endZip){
				return true;
			}
			
			foreach($this->zips[$currentZip]->neighborList as $neighbor){
				if($neighbor == $endZip){
					return true;
				}
				$searchable = false;
				$neighborFoundInStack = false;
				$alreadyVisited = false;
				foreach($searchableZips as $searchableZip){
					if ($neighbor == $searchableZip){
						$searchable = true;
						foreach($stack as $zipNum){
							if ($neighbor == $zipNum){
								$neighborFoundInStack = true;
							}
						}
						if($neighborFoundInStack == false){
							foreach($visitedZips as $visitedZip){
								if($neighbor == $visitedZip){
									$alreadyVisited = true;
								}
							}
						}
					}
				}
				
				if($searchable == true && $neighborFoundInStack == false && $alreadyVisited == false){
					array_push($stack,$neighbor);
				}
			}
		}
		return false;
	}
	
	/**
	Calculates center of district.
	@param $districtNumber Dictrict number to find center of.
	*/
	public function calculateCentroid($districtNumber){
		$coordinates = array();
		
		foreach($this->districts[$districtNumber]->zips as $zip){
			array_push($coordinates, array($this->zips[$zip]->lat, $this->zips[$zip]->lng));
		}

		$numberOfCoordinates = count($coordinates);

		$X = 0.0;
		$Y = 0.0;
		$Z = 0.0;

		foreach ($coordinates as $coord){
			$lat = $coord[0] * pi() / 180;
			$lon = $coord[1] * pi() / 180;

			$a = cos($lat) * cos($lon);
			$b = cos($lat) * sin($lon);
			$c = sin($lat);

			$X += $a;
			$Y += $b;
			$Z += $c;
		}

		$X /= $numberOfCoordinates;
		$Y /= $numberOfCoordinates;
		$Z /= $numberOfCoordinates;

		$lon = atan2($Y, $X);
		$hyp = sqrt($X * $X + $Y * $Y);
		$lat = atan2($Z, $hyp);

		return array($lat * 180 / pi(), $lon * 180 / pi());
	}
	
	/**
	Results string.
	toString returns an organized string that describes the state and its districts used in testing.
	*/
	public function toString(){
		$string = "";
		$string .= $this->state;
		$string .= "<br>";
		$string .= "Total Population: " . $this->population . "<br>";
		$string .= "Required Number of Districts: " . $this->numberOfDistricts . "<br>";
		$string .= "Accepted Population Range Per District: " . round($this->desiredPopulationPerDistrict*.8) . " - " . round($this->desiredPopulationPerDistrict*1.2);
		$string .= "<br>";
		foreach($this->districts as $key=>$value){
			$string .= "<br>";
			$string .= $value->toString();
		}
		return $string;
	}
	
}

?>