paceOptions = {
  ajax: true,
  target: '#map_canvas'
};
// Enable the visual refresh
google.maps.visualRefresh = true; /**< Enable visual refresh for the map. */
var geoXml = null;  /**< GeoXML3 Parser. */
var geoXmlDoc; /**< GeoXML3 Document. */
var map; /**< Google Map Object */
var colorList = ["#00FF00", "#0000FF", "#FF0000", "#01FFFE", "#FFA6FE",
  "#FFDB66", "#006401", "#010067", "#95003A", "#007DB5", "#FF00F6", "#FFEEE8",
  "#774D00", "#90FB92", "#0076FF", "#D5FF00", "#FF937E", "#6A826C", "#FF029D",
  "#FE8900", "#7A4782", "#7E2DD2", "#85A900", "#FF0056", "#A42400", "#00AE7E",
  "#683D3B", "#BDC6FF", "#263400", "#BDD393", "#00B917", "#9E008E", "#001544",
  "#C28C9F", "#FF74A3", "#01D0FF", "#004754", "#E56FFE", "#788231", "#0E4CA1",
  "#91D0CB", "#BE9970", "#968AE8", "#BB8800", "#43002C", "#DEFF74", "#00FFC6",
  "#FFE502", "#620E00", "#008F9C", "#98FF52", "#7544B1"]; /**< List of colors for districts. */
var stateDistricts = [
  {state: "CA", num_dist: 53}, {state: "TX", num_dist: 36}, {state: "NY", num_dist: 27},
  {state: "FL", num_dist: 27}, {state: "IL", num_dist: 18}, {state: "PA", num_dist: 16},
  {state: "OH", num_dist: 16}, {state: "GA", num_dist: 16}, {state: "MI", num_dist: 14},
  {state: "NC", num_dist: 13}, {state: "NJ", num_dist: 12}, {state: "VA", num_dist: 11},
  {state: "WA", num_dist: 10}, {state: "MA", num_dist: 9},  {state: "AZ", num_dist: 9},
  {state: "IN", num_dist: 9},  {state: "TN", num_dist: 9},  {state: "MO", num_dist: 8},
  {state: "MD", num_dist: 8},  {state: "MN", num_dist: 8},  {state: "WI", num_dist: 8},
  {state: "CO", num_dist: 7},  {state: "AL", num_dist: 7},  {state: "SC", num_dist: 7},
  {state: "LA", num_dist: 6},  {state: "KY", num_dist: 6},  {state: "OR", num_dist: 5},
  {state: "OK", num_dist: 5},  {state: "CT", num_dist: 5},  {state: "IA", num_dist: 4},
  {state: "MS", num_dist: 4},  {state: "AR", num_dist: 4},  {state: "KS", num_dist: 4},
  {state: "UT", num_dist: 4},  {state: "NV", num_dist: 4},  {state: "NM", num_dist: 3},
  {state: "NE", num_dist: 3},  {state: "WV", num_dist: 3},  {state: "ID", num_dist: 2},
  {state: "HI", num_dist: 2},  {state: "ME", num_dist: 2},  {state: "NH", num_dist: 2},
  {state: "RI", num_dist: 2},  {state: "WY", num_dist: 1},  {state: "VT", num_dist: 1},
  {state: "ND", num_dist: 1},  {state: "AK", num_dist: 1},  {state: "SD", num_dist: 1},
  {state: "DE", num_dist: 1},  {state: "MT", num_dist: 1},  {state: "DC", num_dist: 0}
  ]; /**< List of states and the number of Congressional Districts. */
var highlightData = []; /**< Array of offsets. Array contains objects with offsets for placemarks within the GeoXML3 document. */
var sidebarHtml = ""; /**< HTML for results. */
var PHPresult; /**< JSON results from backend. */
var highlightOptions = {fillColor: "#FFFF00", strokeColor: "#000000", fillOpacity: 0.9, strokeWidth: 10}; /**< Style options for highlight fill color. */
var highlightLineOptions = {strokeColor: "#FFFF00", strokeWidth: 10}; /**< Style options for highlight line color */

var MapsLib = MapsLib || {};
//MapsLib Object with map variables for initialize/search/reset functions
/**
 * MapsLib Object.
 * Contains many variables and functions responsible for displaying and coloring the districts.
 * @type {{USFusionTableId: string, locationColumn: string, map_centroid: google.maps.LatLng, map_layer: google.maps.FusionTablesLayer, initialize: Function, doSearch: Function, useTheData: Function, failureToParse: Function}}
 */
var MapsLib = {
  USFusionTableId: "16V_qVAMANOzJhLWI3a_58eSQ0kkYmpI5AOvRHOqk",
  locationColumn:     "geometry",
  map_centroid:       new google.maps.LatLng(39.50 ,-98.35), //center that your map defaults to
  map_layer:          new google.maps.FusionTablesLayer(),
  /**
   * Initializes/resets the map.
   * Creates the initial layout of the map and overlay of the US.
  */
  initialize: function() {
    //Create geocoder object
    geocoder = new google.maps.Geocoder(); /**< Google Maps goecoder object. */
    var myOptions = {
      zoom: 4,
      center: MapsLib.map_centroid,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }; /**< Default Google Maps options. */
    //Create map using default options
    map = new google.maps.Map($("#map_canvas")[0],myOptions);
    //Create US States overlay layer for map initialization/reset
    MapsLib.map_layer = new google.maps.FusionTablesLayer(
        {//USA State grey border/fill overlay layer
          suppressInfoWindows: true,
          query: {select: MapsLib.locationColumn, from: MapsLib.USFusionTableId},
          //Apply grey borders and light grey fill
          styles: [
            {polygonOptions: {fillColor: '#FCFCF4', fillOpacity: 0.1}},
            {where: 'Color = 10' , polygonOptions: {fillColor: '#00FF00',fillOpacity:0.3}},
            {where: 'Color = 10' , polygonOptions: {fillColor: '#752b4b',fillOpacity:0.3}}
          ]
        }
    );
    //Apply layer to the map
    MapsLib.map_layer.setMap(map);
    //Create map listener to resize the map on window resize
    google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(MapsLib.map_centroid);
    });
  },

/*
Function: doSearch
Use: Uses the state value in the state selection box, maps the state on the map
 */
  /**
   * Redistricts a state.
   * This is the main function called to start the redistricting and mapping of a state.
   */
  doSearch: function() {
    document.getElementById("status_box").innerHTML = '<h4>Recalculating Districts...</h4>';
    //Clear map layer.
    MapsLib.map_layer.setMap(null);
    if(geoXml){
      geoXml.hideDocument(geoXml.docs[1]);
    }
    //Get state value.
    var address = $("#select_state").val(); /**< State string. */
    var stateObject = null; /**< State object pulled from stateDistricts array. @see stateDistricts */
    //Get stateObject.
    for(var i = 0; i < stateDistricts.length; i++){
      if(address == stateDistricts[i].state){
        stateObject = stateDistricts[i];
        break;
      }
    }
    highlightData = [];
    //Create sub arrays equal to the number of districts in the state.
    for (var i = 0; i <= stateObject.num_dist; i++){
      highlightData.push([]);
    }
    //Call the server-side redistricting algorithm.
    if(address == "LA") alert("Corrupted kml file. Please refresh page and choose a different state.");
    if(address == "MO") alert("Failed to parse data. Please refresh page and choose a different state.");
    if(address == "WY" || address == "VT" || address == "ND" || address == "AK" || address == "SD" || address == "DE" || address == "MT"){
      geoXml = new geoXML3.parser({
        map: map,
        processStyles: false,
        singleInfoWindow: true,
        zoom: true,
        afterParse: MapsLib.useTheData,
        failedParse: MapsLib.failureToParse
      });
      //Create map listener to check that parser has been successfully created.
      google.maps.event.addListener(geoXml,'parsed', createResults(address));
      //Update status bar.
      document.getElementById("status_box").innerHTML = '<h4 align="center">...Loading Zipcode Boundary Data...</h4>';
      //Parse the state kml data file.
      geoXml.parse('kml/' + address + '.kml');
    } else {
      $.when(callPHP(stateObject)).done(function(ajaxResult){
        PHPresult = ajaxResult;
        geoXml = new geoXML3.parser({
          map: map,
          processStyles: false,
          singleInfoWindow: true,
          zoom: true,
          afterParse: MapsLib.useTheData,
          failedParse: MapsLib.failureToParse
        });
        //Create map listener to check that parser has been successfully created.
        google.maps.event.addListener(geoXml,'parsed', createResults(address));
        //Update status bar.
        document.getElementById("status_box").innerHTML = '<h4 align="center">...Loading Zipcode Boundary Data...</h4>';
        //Parse the state kml data file.
        geoXml.parse('kml/' + address + '.kml');
      });
    }


  },
  /**
   * Colors and displays the districts.
   * Receives the parsed kml files and sort data form back end and applies the coloration for the districts.
   * @see MapsLib.doSearch()
   * @param doc the GeoXML3 document returned from the parser.
   */
  useTheData: function(doc){
    //Update status bar.
    document.getElementById("status_box").innerHTML = '<h4 align="right">...Mapping Districts</h4>';
    var address = $("#select_state").val(); /**< State string. */
    geoXmlDoc = doc;
    //Parse GeoXML3 document.
    for (var j = 0; j<geoXmlDoc.length;j++) {
      if (!geoXmlDoc[j] || !geoXmlDoc[j].placemarks || !geoXmlDoc[j].placemarks.length)
        continue;
      for (var i = 0; i < geoXmlDoc[j].placemarks.length; i++) {
        var placemark = geoXmlDoc[j].placemarks[i];
        if (placemark.polygon) {
          var kmlFillColor = {color: "#ff00ff", opacity: 0.45}; /**< Default fill color. */
          var normalStyle = {
            strokeColor: "#000000",
            strokeWeight: placemark.style.width,
            strokeOpacity: 1,
            fillColor: kmlFillColor.color,
            fillOpacity: kmlFillColor.opacity
          }; /**< Default normal style */
          placemark.polygon.normalStyle = normalStyle;


          //Coloration of polygon is done here.
          //Unique 1 District state case.
          if(address == "WY" || address == "VT" || address == "ND" || address == "AK" || address == "SD" || address == "DE" || address == "MT"){
            placemark.polygon.setOptions({fillColor: colorList[0], strokeColor: "#000000", fillOpacity: 0.75})
            placemark.polygon.normalStyle = {fillColor: colorList[0], strokeColor: "#000000", fillOpacity: 0.75}
            highlightData[0].push({docOffset:j, placemarkOffset:i});
          }else{ //All other states.
            var zipObject = searchJSON(placemark.description, PHPresult); /**< Zip code result from server */
            if(zipObject){
              placemark.polygon.setOptions({fillColor: colorList[zipObject.District-1], strokeColor: "#000000", fillOpacity: 0.75});
              placemark.polygon.normalStyle = {fillColor: colorList[zipObject.District-1], strokeColor: "#000000", fillOpacity: 0.75}
              highlightData[zipObject.District-1].push({docOffset:j, placemarkOffset:i});
            }else{
              placemark.polygon.setOptions({fillColor: "#000000", strokeColor: "#FFFFFF", fillOpacity: 0.99})
              placemark.polygon.normalStyle = {fillColor: "#000000", strokeColor: "#FFFFFF", fillOpacity: 0.99}
            }
          }
        }
      }
    }
    //Hide status bar.
    setTimeout(document.getElementById("status_box").style.display="none",10000);
  },
  /**
   * Failure case for invalid XML file.
   * @see MapsLib.doSearch()
   * @param doc GeoXML3 failed parse result.
   */
  failureToParse: function(doc){
    alert("Failed to parse kml file")
  }
};

/**
 * Highlights a district.
 * Given a district number, this will highlight all zipcodes assigned to this district.
 * @param pm District number to highlight.
 */
function kmlHighlightPoly(pm) {
  for (var i = 0; i <= highlightData[pm].length; i++){
    var placemark = geoXmlDoc[highlightData[pm][i].docOffset].placemarks[highlightData[pm][i].placemarkOffset];
    if (placemark.polygon) placemark.polygon.setOptions(highlightOptions);
  }
}

/**
 * Unhighlights a district.
 * Returns a district and all of its zips to normal style.
 * @param pm District to unhighlight.
 */
function kmlUnHighlightPoly(pm) {
  for (var i = 0; i <= highlightData[pm].length; i++){
    var placemark = geoXmlDoc[highlightData[pm][i].docOffset].placemarks[highlightData[pm][i].placemarkOffset];
    if (placemark.polygon) placemark.polygon.setOptions(placemark.polygon.normalStyle);
  }
}

/**
 * Search redistricting results.
 * Searches through the results from the redistricting algorithm and returns an object with the zipcode and district.
 * @param zipcode Zipcode to search for.
 * @param json JSON file to search through.
 * @returns {*} Return object with zipcode and district.
 */
function searchJSON(zipcode, json){
  for(var i = 0; i < json.length; i++){
    if(zipcode == json[i].Zip_Code && json[i].District != null){
      return json[i];
    }
  }
  return null;
}

/**
 * Creates results box.
 * Creates and shows results box with the districts.
 * @param address State string.
 */
function createResults(address){
  var num = 0; /**< Number of districts. */
  //Create beginning of results html string.
  sidebarHtml = '<h4>Results:</h4><table style="width: 100%">';
  //Find the number of districts in the state.
  for(var i = 0; i < stateDistricts.length; i++){
    if(address == stateDistricts[i].state){
      num = stateDistricts[i].num_dist;
    }
  }
  //Create rows of districts.
  for(var i = 0; i < num; i++){
    if(i % 5 == 0){
      sidebarHtml += '<tr id="row'+i+'">'
    }
    sidebarHtml += '<td style="padding : 0.5em;"onmouseover="kmlHighlightPoly('+i+');" onmouseout="kmlUnHighlightPoly('+i+');">District '+(i+1)+'</td>';
    //style="padding : 0.5em 0.8em;"
    if(i % 5 == 4){
      sidebarHtml += '</tr>'
    }
  }
  if(sidebarHtml.slice(-4,-1) != '</tr>')
  sidebarHtml += "</table>";
  //Update results display.
  document.getElementById("result_box").innerHTML = sidebarHtml;
}

/**
 * Calls the redistricting algorithm.
 * Makes an AJAX call to the server to redistrict the given state.
 * @param stateObject Object with state and number of districts.
 * @returns {*} Results of redistricting algorithm.
 */
function callPHP(stateObject) {
  return $.ajax({//Make the Ajax Request
    type: "POST",
    url: "back end/redistrict.php",
    'dataType': "json",
    data: stateObject
    }
  );
}