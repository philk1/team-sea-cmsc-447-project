# README #

### What is this for? ###

* Quick summary

	* This project is dedicated to the creation of a US Congressional Redistricting Web Applet for the UMBC CMSC 447 Class.
	
* Version 1

### How do I get set up? ###

* Summary of set up

	* This program can be run locally though WAMP or a similar application.
	* Download the files and place in your working directory of your server.
	* Must import the team_sea_447_project.sql file into a new database named as "team sea 447 project".
	
* Configuration
	
	* Main html file: index.html
	* Main mapping file: js/maps_lib.js
    * Main redistricting file: back end/redistrict.php
    * Main front end data location: kml/*
    * Main back end data location: team_sea_447_project.sql
	
* Dependencies
	
	* php
	* javascript
	* mySQL
	* Working internet connection
	
* Database configuration
	
	* Default credentials may need to be edited within back end/redistrict.php to work with your mySQL server setup.

* How to run tests

	* If you followed the steps above, the url should be localhost/your_working_directory or some similar iteration.

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact