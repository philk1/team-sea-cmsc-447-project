var searchData=
[
  ['_24currentpopulation',['$currentPopulation',['../class_district.html#a3a33f39da7b9d4f81f483c74db69601c',1,'District']]],
  ['_24desiredpopulationperdistrict',['$desiredPopulationPerDistrict',['../class_state.html#a60befc1c6a3c7739ce94dfacd037e61c',1,'State']]],
  ['_24districtnumber',['$districtNumber',['../class_zip_code.html#a40d8c53654e8f1efd55002497404b4c5',1,'ZipCode\$districtNumber()'],['../class_district.html#a40d8c53654e8f1efd55002497404b4c5',1,'District\$districtNumber()']]],
  ['_24districts',['$districts',['../class_state.html#a9e04dbf34bf2b3742496024a90327b39',1,'State']]],
  ['_24lat',['$lat',['../class_zip_code.html#af498b42b83afed4dfe0af05fd802776c',1,'ZipCode']]],
  ['_24lng',['$lng',['../class_zip_code.html#ab27bfa6a9380a8d89e1a8e001b276410',1,'ZipCode']]],
  ['_24neighborlist',['$neighborList',['../class_zip_code.html#ace67e890442e3547ae0ca9e0a15e040c',1,'ZipCode']]],
  ['_24numberofdistricts',['$numberOfDistricts',['../class_state.html#a5621c7011b4032e99fe9305978747c66',1,'State']]],
  ['_24population',['$population',['../class_zip_code.html#afc1939ed7d0e8629546e2bc27b02dbc1',1,'ZipCode\$population()'],['../class_state.html#afc1939ed7d0e8629546e2bc27b02dbc1',1,'State\$population()']]],
  ['_24startingzipcodes',['$startingZipCodes',['../class_state.html#a1a32f3327c64ded0750c851f7f44d643',1,'State']]],
  ['_24state',['$state',['../class_zip_code.html#ae82306c4f2d17d8dd5c7d8d916b33bed',1,'ZipCode\$state()'],['../class_district.html#ae82306c4f2d17d8dd5c7d8d916b33bed',1,'District\$state()'],['../class_state.html#ae82306c4f2d17d8dd5c7d8d916b33bed',1,'State\$state()']]],
  ['_24zip',['$zip',['../class_zip_code.html#aa91be3142812d8cd4221c6f54555079b',1,'ZipCode']]],
  ['_24zips',['$zips',['../class_district.html#a013e9c074e8247ab4018d88bea27c6b5',1,'District\$zips()'],['../class_state.html#a013e9c074e8247ab4018d88bea27c6b5',1,'State\$zips()']]]
];
